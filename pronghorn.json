{
  "id": "@itentialopensource/adapter-db_mongo",
  "type": "Adapter",
  "export": "DBMongo",
  "displayName": "DBMongo",
  "title": "DBMongo",
  "src": "adapter.js",
  "roles": [
      "admin"
  ],
  "methods": [
    {
      "name": "create",
      "summary": "Create an item in the collection in the database",
      "description": "Create an item in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "data",
          "type": "object",
          "info": "The data object to add to the collection",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/create"
      },
      "task": true
    },
    {
      "name": "createMany",
      "summary": "Create items in the collection in the database",
      "description": "Create items in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "data",
          "type": "array",
          "info": "The data objects to add to the collection",
          "required": true
        },
        {
          "name": "ordered",
          "type": "boolean",
          "info": "Whether the data should be ordered",
          "required": false
        },
        {
          "name": "writeConcern",
          "type": "string",
          "info": "The write concern",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/createMany"
      },
      "task": true
    },
    {
      "name": "collectionSize",
      "summary": "Get the size of the collection",
      "description": "Get the size of the collection",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "the collection to get",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/collectionSize"
      },
      "task": true
    },
    {
      "name": "query",
      "summary": "Query items from the collection in the database",
      "description": "Query items from the collection in the database",
      "input": [
        {
          "name": "queryDoc",
          "type": "object",
          "info": "queryDoc = { collection : <collection_name>, filter : <filter Obj>, projection : <projection Obj>, sort : <sort Obj>, start : <start position>, limit : <limit of results> }",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/query"
      },
      "task": true
    },
    {
      "name": "queryWithOptions",
      "summary": "Query items from the collection in the database",
      "description": "Query items from the collection in the database",
      "input": [
        {
          "name": "queryDoc",
          "type": "object",
          "info": "queryDoc = { collection : <collection_name>, filter : <filter Obj>, projection : <projection Obj>, sort : <sort Obj>, start : <start position>, limit : <limit of results> }",
          "required": true
        },
        {
          "name": "findOptions",
          "type": "object",
          "info": "findOptions = { allowDiskUse: boolean, ... }",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/query"
      },
      "task": true
    },
    {
      "name": "find",
      "summary": "Find items from the collection in the database",
      "description": "Find items from the collection in the database",
      "input": [
        {
          "name": "options",
          "type": "object",
          "info": "options = { entity : <collection_name>, filter : <filter Obj>, sort : <sort Obj>, start : <start position>, limit : <limit of results> }",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/find"
      },
      "task": true
    },
    {
      "name": "count",
      "summary": "Find count of items from the collection in the database",
      "description": "Find count of items from the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "filter",
          "type": "object",
          "info": "The filter for data in the collection",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/count"
      },
      "task": true
    },
    {
      "name": "aggregate",
      "summary": "Run the aggregate on the collection in the database",
      "description": "Run the aggregate on the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "aggregations",
          "type": "object",
          "info": "The aggregations to run on the collection",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/aggregate"
      },
      "task": true
    },
    {
      "name": "search",
      "summary": "Search for items in the collection in the database",
      "description": "Search for items in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "filter",
          "type": "object",
          "info": "The filter for data in the collection",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/search"
      },
      "task": true
    },
    {
      "name": "distinct",
      "summary": "Search for distinct items in the collection in the database",
      "description": "Search for distinct items in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "field",
          "type": "string",
          "info": "The distinct field in the collection",
          "required": true
        },
        {
          "name": "query",
          "type": "object",
          "info": "The query for data in the collection",
          "required": false
        },
        {
          "name": "options",
          "type": "object",
          "info": "The options to apply to the query",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/distinct"
      },
      "task": true
    },
    {
      "name": "exists",
      "summary": "Find if items exist in the collection in the database",
      "description": "Find if items exist in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "query",
          "type": "object",
          "info": "The query for data in the collection",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/exists"
      },
      "task": true
    },
    {
      "name": "filterFields",
      "summary": "Find fields from items in the collection in the database",
      "description": "Find fields from items in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "filter",
          "type": "object",
          "info": "The filter for data in the collection",
          "required": false
        },
        {
          "name": "fields",
          "type": "object",
          "info": "The fields to get for data in the collection",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/filterFields"
      },
      "task": true
    },
    {
      "name": "sortQuery",
      "summary": "Query items from the collection in the database",
      "description": "Query items from the collection in the database",
      "input": [
        {
          "name": "queryDoc",
          "type": "object",
          "info": "queryDoc = { collectionName : <collection_name>, filter : <filter Obj>, projection : <projected fields>, cursor1 : <>, cursor1value : <>, cursor2 : <>, cursor2value : <> }",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/sortQuery"
      },
      "task": true
    },
    {
      "name": "searchById",
      "summary": "Search for item by id in the collection in the database",
      "description": "Search for item by id in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "id",
          "type": "string",
          "info": "The id of the item in the collection",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/searchById"
      },
      "task": true
    },
    {
      "name": "save",
      "summary": "Save an item in the collection in the database",
      "description": "Save an item item by id in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "id",
          "type": "string",
          "info": "The id of the item in the collection",
          "required": true
        },
        {
          "name": "data",
          "type": "object",
          "info": "The data to put into the collection",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/save"
      },
      "task": true
    },
    {
      "name": "updateSearched",
      "summary": "Update an item in the collection in the database",
      "description": "Update an item item by id in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "filter",
          "type": "object",
          "info": "The filter to find the item in the collection",
          "required": false
        },
        {
          "name": "data",
          "type": "object",
          "info": "The data to update into the collection",
          "required": true
        },
        {
          "name": "multi",
          "type": "boolean",
          "info": "Whether to update one or many",
          "required": false
        },
        {
          "name": "upsert",
          "type": "boolean",
          "info": "Whether to insert new objects",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/updateSearched"
      },
      "task": true
    },
    {
      "name": "findAndModify",
      "summary": "Update an item in the collection in the database",
      "description": "Update an item item by id in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "filter",
          "type": "object",
          "info": "The filter to find the item in the collection",
          "required": false
        },
        {
          "name": "sort",
          "type": "object",
          "info": "The sort to find the item in the collection",
          "required": false
        },
        {
          "name": "data",
          "type": "object",
          "info": "The data to update into the collection",
          "required": true
        },
        {
          "name": "multi",
          "type": "boolean",
          "info": "Whether to update one or many",
          "required": false
        },
        {
          "name": "upsert",
          "type": "boolean",
          "info": "Whether to insert new objects",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/findAndModify"
      },
      "task": true
    },
    {
      "name": "deleteById",
      "summary": "Delete an item in the collection in the database",
      "description": "Delete an item by id in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "id",
          "type": "string",
          "info": "The id of the item to delete in the collection",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/deleteById"
      },
      "task": true
    },
    {
      "name": "deleteSearched",
      "summary": "Delete items in the collection in the database",
      "description": "Delete items by id in the collection in the database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "filter",
          "type": "object",
          "info": "The filter to find items to delete in the collection",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/deleteSearched"
      },
      "task": true
    },
    {
      "name": "reserveNextRecord",
      "summary": "Reserve the next record in the mongo",
      "description": "",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "filter",
          "type": "object",
          "info": "The filter for data in the collection",
          "required": false
        },
        {
          "name": "reserveString",
          "type": "string",
          "info": "Reserve string",
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/reserveNextRecord"
      },
      "task": true
    },
    {
      "name": "reserveNextID",
      "summary": "Add and reserve the next id in mongo",
      "description": "Add and reserve the next id in mongo",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "Name of the collection the data goes in",
          "required": true
        },
        {
          "name": "filter",
          "type": "object",
          "info": "The filter for data in the collection",
          "required": false
        },
        {
          "name": "recordInfo",
          "type": "object",
          "info": "The payload for record",
          "required": true
        },
        {
          "name": "keyLabel",
          "type": "string",
          "info": "The name of the key",
          "required": true
        },
        {
          "name": "start",
          "type": "number",
          "info": "The filter for data in the collection",
          "required": true
        },
        {
          "name": "end",
          "type": "number",
          "info": "The filter for data in the collection",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/reserveNextID"
      },
      "task": true
    },
    {
      "name": "listCollections",
      "summary": "List collections of the current database",
      "description": "List collections of the current database",
      "input": [
        {
          "name": "filter",
          "type": "object",
          "info": "an optional filter document to determine which collecitons to return",
          "required": false
        },
        {
          "name": "options",
          "type": "object",
          "info": "optional command options",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/listCollections"
      },
      "task": true
    },
    {
      "name": "createCollection",
      "summary": "Create a collection in the current database",
      "description": "Create a collection in the current database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "the name of the colleciton to create",
          "required": true
        },
        {
          "name": "options",
          "type": "object",
          "info": "optional command options",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/createCollection"
      },
      "task": true
    },
    {
      "name": "dropCollection",
      "summary": "Drop a collection from the current database",
      "description": "Drop a collection from the current database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "the name of the colleciton to drop",
          "required": true
        },
        {
          "name": "options",
          "type": "object",
          "info": "optional command options",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/dropCollection"
      },
     "task": true
    },
    {
      "name": "renameCollection",
      "summary": "Rename a collection in the current database",
      "description": "Rename a collection in the current database",
      "input": [
        {
          "name": "collectionName",
          "type": "string",
          "info": "the name of the colleciton to rename",
          "required": true
        },
        {
          "name": "newCollectionName",
          "type": "string",
          "info": "the new name for the colleciton",
          "required": true
        },
        {
          "name": "options",
          "type": "object",
          "info": "optional command options",
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result"
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/renameCollection"
      },
      "task": true
    }
  ]
}
