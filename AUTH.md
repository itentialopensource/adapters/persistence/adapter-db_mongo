## Authenticating Mongo DB Adapter 

This document will go through the steps for authenticating the Mongo DB adapter with. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

### Database Library Authentication
The Mongo DB adapter requires Database Library Authentication. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to an Adapter for Mongo DB server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for a cluster set up. Tweak the properties as needed for your set up.
```json
    "properties": {
      "db": "<databaseName>",
      "host": "host1:27017,host2:27017,host3",
      "port": 27017,
      "createUuid": true,
      "databaseConnection": "connect on startup",
      "credentials": {
        "dbAuth": true,
        "dbAuthSource": "admin",
        "passwd": "<password>",
        "user": "<username>"
      },
      "ssl": {
        "enabled": true,
        "sslValidate": true,
        "sslCA": "/home/dirA/dirB/myCAFile.cert",
        "acceptInvalidCerts": true,
        "checkServerIdentity": true
      },
      "replSet": {
        "enabled": true,
        "replicaSet": "replicaSet=rs1"
      }
    }
```
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct username and password.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Investigate the logs
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.