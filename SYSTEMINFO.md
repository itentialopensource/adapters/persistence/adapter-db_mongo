# MongoDB

Vendor: Mongo
Homepage: https://www.mongodb.com/

Product: MongoDB
Product Page: https://www.mongodb.com/

## Introduction
We classify MongoDB into the Data Storage domaina as MongoDB is a database which provides the storage of information. 

"Loved by developers"
"The world's most popular document database is now the world's most versatile developer data platform."

## Why Integrate
The MongoDB adapter from Itential is used to integrate the Itential Automation Platform (IAP) with MongoDB. With this adapter you have the ability to perform operations with MongoDB on items such as:

- Storage of Information
- Retrieval of Information

## Additional Product Documentation
The [MongoDB Collection methods](https://www.mongodb.com/docs/manual/reference/method/js-collection/)
The [MongoDB Node Library Documentation](https://www.npmjs.com/package/mongodb)