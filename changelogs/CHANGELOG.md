
## 0.5.7 [06-29-2023]

* Remove any sensitive data logging

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!30

---

## 0.5.6 [06-22-2023]

* Remove any sensitive data logging

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!30

---

## 0.5.5 [05-01-2023]

* updated mongo driver node version

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!29

---

## 0.5.4 [11-22-2022]

* adding auth.md file

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!27

---

## 0.5.3 [07-11-2022]

* Add a choice of DB connection establishment

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!26

---

## 0.5.2 [05-04-2022]

* expose collection management crud methods

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!25

---

## 0.5.1 [04-16-2022]

* fix some calls for ObjectId and add routes

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!24

---

## 0.5.0 [02-24-2022]

* changes to bypass healthcheck

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!23

---

## 0.4.13 [12-28-2021]

* Patch/adapt 1885

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!22

---

## 0.4.12 [11-15-2021]

* Add notes int he README about complex filters

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!21

---

## 0.4.11 [11-01-2021]

* changed data param from type object to type array for createMany task in pronghorn.js

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!20

---

## 0.4.10 [10-18-2021]

* Add documentation to readme for property examples

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!18

---

## 0.4.9 [10-14-2021]

* attempt to fix npm i issue with npx

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!19

---

## 0.4.8 [10-13-2021]

* Allow username and password to be added to the url

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!17

---

## 0.4.7 [10-13-2021]

* Changes for replicaSet

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!16

---

## 0.4.6 [08-02-2021]

* changes for IAP sending empty strings

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!15

---

## 0.4.5 [07-22-2021]

* Add support for sort, limit, skip in query method

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!14

---

## 0.4.4 [07-22-2021]

* Remove default value for url from propertiesSchema

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!11

---

## 0.4.3 [07-21-2021]

* Change mongodb version

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!12

---

## 0.4.2 [07-21-2021]

* Allow filter to be a string in find method

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!13

---

## 0.4.1 [04-07-2021]

* changes so that we can support all mongo options on the url

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!10

---

## 0.4.0 [11-25-2020]

* add the ability to not set the id and have the database do that on creates

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!9

---

## 0.3.0 [10-06-2020]

* Parse string representation of objects in method parameters

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!8

---

## 0.2.2 [02-25-2020]

* Added the  atomic property to the update request data. If sort is not passed...

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!5

---

## 0.2.1 [11-21-2019]

* Patch/date time

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!4

---

## 0.2.0 [11-14-2019]

* changes to the connect method for url and property handling

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!3

---

## 0.1.7 [10-08-2019]

* Patch/auth

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!2

---

## 0.1.6 [10-07-2019]

* Patch/auth

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!2

---

## 0.1.5 [10-03-2019]

* Added mongo services

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!1

---

## 0.1.4 [09-23-2019]

* Added mongo services

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!1

---

## 0.1.3 [08-13-2019]

* Added mongo services

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!1

---

## 0.1.2 [08-13-2019]

* Added mongo services

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!1

---

## 0.1.1 [08-09-2019]

* Bug fixes and performance improvements

See commit 9802246

---
