Mongo Adapter
===

This adapter allows interaction with a mongodb server/cluster. For efficiency, this adapter should only be used in IAP workflow calls. Calling the adapter from Applications instead of using the npm mongodb pacakges will be less efficient!

License & Maintainers
---

### Maintained by:

Itential Adapter Team

Check the [changelog](CHANGELOG.md) for the latest changes.

### License

Itential, LLC proprietary

### Compatible Mongodb Versions

Please refer to [mongodb documentation](https://www.mongodb.com/docs/drivers/node/current/compatibility/#language-compatibility) to see compatibility matrix. Node js driver version can be found in package.json of the adapter, under dependencies.

## Example properties for Host, SSL and ReplicaSet

### Host

If you have a Mongo conluster configured, you can define multiple hosts using the url property. However, if you would like to use the host property, you need to define the host in an unusual manner. To specify that there are multiple hosts for a replica set, you need to list them in the host field like so, 

```json 
{
    "host": "host1:27017,host2:27017,host3",
    "port": 27107,
}
```

Note that the port number is not specified for the third and last host. This is a "hack" to inform IAP that there are 3 hosts for this specific adapter. The host string should always be formatted this way where for "x" number of hosts, the last host should not have a specified port number appended.

Typically, for regular hosts, the host and port are specified separately, as show below. 

```json
{
    "host": "host1",
    "port": 27017, 
    "replSet": {
        "enabled": false,
        "replicaSet": ""
    }
}
```
Note that if using the url property rather than the host property, the url must be complete. This means that the URL will need to contain the port as well as database name. Below is an example using host `host1` and database `myDB`:
```json 
{
    "url" : "mongodb://host1:27017/myDB"
}
```
If the database is not specified it will default to `admin`.

| Property | Description |
| ------- | ------- |
| host | The host for Mongo we are talking to. Can be multiple hosts as shown above.|
| port | The port for the host we are talking to.|

### SSL
The SSL section defines the properties utilized for ssl authentication with MongoDB.
SSL can work two different ways: set the `acceptInvalidCerts` flag to true (only recommended for lab environments), or provide a CA file.
Below is an example of what valid SSL settings may look like when SSL is disabled.

```json
"ssl": {
    "enabled": false,
    "acceptInvalidCerts": false,
    "sslCA": "",
    "checkServerIdentity": false,
}
```

Below is an example of what valid SSL settings may look like when SSL is enabled.

```json
"ssl": {
    "enabled": true,
    "acceptInvalidCerts": false,
    "sslCA": "/home/dirA/dirB/myCAFile.cert",
    "checkServerIdentity": false
}
```

| Property | Description |
| ------- | ------- |
| enabled | If SSL is required, set to true. |
| acceptInvalidCerts | Defines if the adapter should accept invalid certificates (only recommended for lab environments). Required if SSL is enabled. Default is false.|
| sslCA | Defines the path name to the CA file used for SSL. If SSL is enabled and the accept invalid certifications is false, then ca_file is required.|
| checkServerIdentity | Defines if the certificate should be validated against the url. Default is false.|

### ReplicaSet

If your Mongo adapter is configured to have a replica set, the properties should also be configured to include the replica set information. You would need to either provide this information in the url if you are using the url property or provide the information in the replicaSet section  of the properties. For the url, the name of the replica set should also be appended to the url as shown.

```json 
{
    "url": "mongodb://host1:27017,host2:27017,host3:27017?replicaSet=rs1"
}

```

If you are using individual properties, you would enable the replSet and provide the information for the replica set as follows: 

```json 
{
    "replSet": {
        "enabled": true,
        "replicaSet": "replicaSet=rs1"
    }
}
```

Below are examples of what valid replicaSet settings may look like within the mongoDB 
adapter. It should be noted that in the past, the replicaSet field had to be set to "replicaSet=rs1"; however, that has been fixed to where the passed in parameter can just contain the set name.  

```json 
"replSet": {
    "enabled": false,
    "replicaSet": ""
}
```

Below is that the value for replicaSet had to look like before.

```json
"replSet": {
    "enabled": true,
    "replicaSet": "relicaSet=rs1"
}
```

The replicaSet value can now just be the name of the set.

```json 
"replSet": {
    "enabled": true,
    "replicaSet": "rs1"
}
```

| Property | Description |
| ------- | ------- |
| enabled | Set to true if you are using replica sets and you are not using a url.|
| replicaSet | Defines the name for the replicaSet. Can be provided in various ways as shown above.|

## Managing your Mongo database with the "filter" and "data" fields

The filter and data fields are used by multiple tasks in the Mongo Adapter. Below is an example of how to use these two parameters along with the updateSearched task from this adapter to query and edit your database.

Below are two documents that describes two students, Jane Smith and John Williams, and the classes they would like to take next semester. However, for each class, there are prequisite classes that they need to pass. 

```json
{
    "studentID": 123456,
    "name": "Jane Smith",
    "year:": "Junior",
    "enrolled": true, 
    "requestedClasses": [
        {
            "className": "COMPSCI444",
            "prerequisites": {
                "MATH101": "pass", 
                "COMPSCI101": "fail"
            }, 
        },
        {
            "className": "DANCE123",
            "prerequisites": {
                "GYM456": "pass"
            }, 
        },
        {
            "className": "CHEM121",
            "prerequisites": {
                "BIO351": "pass", 
                "CHEM101": "fail"
            }
        }
    ]  
}

{
    "studentID": 987654,
    "name": "John Williams",
    "year:": "Junior",
    "enrolled": true, 
    "requestedClasses": [
        {
            "className": "COMPSCI999",
            "prerequisites": {
                "COMPSCI444": "pass", 
                "MATH303": "pass"
            }, 
        },
        {
            "className": "PHYS898",
            "prerequisites": {
                "PHYS351": "pass", 
                "MATH303": "pass"
            }
        }
    ]  
}
```
We can use the following filter (which must be an object) to just bring up Jane's profile and ignore John's
```json
{
    "studentID": 123456
}
```
Filtering by any unique field will have the same result (i.e. filtering my name instead of studentID). 

The filter can also specify which fields you want to update, which comes in handy for long, complicated documents. For example, if we want to change Jane's pass/fail status for her MATH101 class, we would first filter out the field(s) we want to change

```json
{
    "name": "Jane Smith",
    "requestedClasses.className": "COMPSCI444"
}
```
First we need to filter by Jane's name, but unlike the previous example, we also need to specify which class from the requestedClasses array we need to change. Therefore, we need to add an additional field to the filter. 

To change the Jane's prerequisite status from fail to pass for COMPSCI1444, the data field would look like
```json 
{
    "requestedClasses.$.prerequisites.COMPSCI444": "pass" 
}
```
The "$" in the data object is a positional operator that identifies an element in the requestedClasses array without having to specify the index or position of the element in the array.

If we pass these values into the filter and data fields in the "updateSearched" task in IAP, it would result in the following document for Jane's profile and she would be able to take COMPSCI444.

```json
{
    "studentID": 123456,
    "name": "Jane Smith",
    "year:": "Junior",
    "enrolled": true, 
    "requestedClasses": [
        {
            "className": "COMPSCI444",
            "prerequisites": {
                "MATH101": "pass", 
                "COMPSCI101": "pass"
            }, 
        },
        {
            "className": "DANCE123",
            "prerequisites": {
                "GYM456": "pass"
            }, 
        },
        {
            "className": "CHEM121",
            "prerequisites": {
                "BIO351": "pass", 
                "CHEM101": "fail" 
            }
        }
    ]  
}
```
## Using Query and Projection Operators

The adapter now supports operators containing the "$" symbol (ex. $gte, $ne, etc). To use these operators for mongo queries, replace the "$" symbol with "{op}" in the filter. Methods within the adapter will handle replacing the "{op}" trigger with "$" before passing the filter to the MongoClient.

For instance, if you would like to find all the documents in your database that contain a "number" field of greater than or equal to 10 (num >= 10), you can set your filter like so 

```json
{
    "number": {
        "{op}gte": 10
    }
}

```

The adapter then reformats the filter before passing it onto the MongoClient
```json
{
    "number": {
        "$gte": 10
    }
}
```

## Returning Data with String Representation of ObjectId Fields

The adapter supports having fields of type ObjectId returned in a stringified representation, rather than returning just the objectId itself. This will allow users to know which fields are of type ObjectId and allow for them to be converted back upon insert if needed. To enable this, add the following property to the service instance configuration properties:

```json
{
    "convertObjectId": true
}
```